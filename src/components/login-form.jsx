import { Field, Form, Formik } from "formik";
import { useContext } from "react";
import { ThemeContext } from "../utils/theme.context";

const LoginForm = ({ onSubmit, myRef }) => {
  const context = useContext(ThemeContext);
  const inicial_data = {
    user_name: "",
    password: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  const text_class = context.theme === "dark" ? "text-white" : "";

  return (
    <Formik
      initialValues={inicial_data}
      onSubmit={submit}
      innerRef={myRef}
    >
      {({}) => {
        return (
          <Form>
            <div className="form-group mb-3">
              <label className={text_class} htmlFor="username">
                Nombre de usuario
              </label>
              <Field
                className="form-control"
                id="username"
                type="text"
                name="user_name"
                placeholder="Ingrese su nombre de usuario"
              />
            </div>
            <div className="form-group mb-3">
              <label className={text_class} htmlFor="pass">
                Contraseña
              </label>
              <Field
                className="form-control"
                id="pass"
                type="password"
                name="password"
                placeholder="Ingrese su contraseña"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default LoginForm;
